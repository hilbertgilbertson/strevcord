<?php
/**
 * StrevCord
 * @file config.php
 * @version 1.0
 * @author HilbertGilbertson
 * @url https://hilbertgilbertson.website
 */

/*
 * Config Parameters
 *
 * appid (int): the Steam AppID to monitor reviews for
 *
 * permitted_languages (array or false): to only post reviews in specific
 * languages, set to an array e.g. array("english", "spanish"). Or set to false
 * to disable and post all reviews regardless of language. See API language
 * codes here: https://partner.steamgames.com/doc/store/localization
 *
 * review_char_limit (int): max length of review description to post; Cannot
 * exceed 2000
 *
 * webhookURL (string): the URL of your Discord webhook
 *
 * avatar (string or false): the image URL of an avatar for your Discord
 * webhook. Set to false to use no avatar or the preconfigured avatar
 *
 * username (string or false): a username for your Discord webhook. Set to
 * false to use the preconfigured username for your webhook
 *
 * runfile (string): path to an empty text file used to store the timestamp of
 * last execution. www-data/apache user must have write access to this file
 *
 * runevery (int): how frequently the script runs (in minutes); set up a
 * cron to match this frequency
 *
 * debug (bool): Don't enable unless you're prepared for a load of data to
 * flood into your webhook channel. For debugging only, hence the name ;-)
 *
 */

$config['appid'] = 12345;
$config['permitted_languages'] = false;
$config['review_char_limit'] = 500;

$config['webhookURL'] = "https://discordapp.com/api/webhooks/12345678901234567/....";
$config['avatar'] = "http://pngimg.com/uploads/smiley/smiley_PNG144.png";
$config['username'] = "StrevCord";

$config['runfile'] = ".scrun";
$config['runevery'] = 10;

$config['debug'] = false;