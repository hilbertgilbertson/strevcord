<?php
/**
 * StrevCord
 * @file StrevCord.php
 * @version 1.0
 * @author HilbertGilbertson
 * @url https://hilbertgilbertson.website
 */

class StrevCord
{
    protected $config;

    public function __construct($config)
    {
        $this->config = (object)$config;
        if(!file_exists($this->config->runfile)){
            if(!touch($this->config->runfile)){
                die("Could not create initial run file. Please ensure the file '{$this->config->runfile}' exists with write permissions for the webserver user.");
            }
            $this->lastran = time() - (60 * ($this->config->runevery + 1));
        } else {
            $runfile = trim(file_get_contents($this->config->runfile));
            if(empty($runfile) || !ctype_digit($runfile)) $runfile = time() - (60 * $this->config->runevery);
            $this->lastran = $runfile;
        }
        file_put_contents($this->config->runfile, time());

        if($this->config->debug) $this->lastran = 0;
    }


    protected function hoursmins($time){
        if ($time < 1) {
            return;
        }
        $hours = floor($time / 60);
        $mins = ($time % 60);
        return ($time >= 60 ? $hours . ' hour'.($time > 119 ? "s " : ' ') : '').($mins > 0 ? "$mins minute".($mins != 1 ? "s" : '') : "");
    }

    protected function webhook_post($url, $avatar, $username, $embeds = null, $message = null)
    {
        if ($embeds !== null || $message != null) {
            if (!empty($username))
                $data['username'] = $username;
            if (!empty($avatar))
                $data['avatar_url'] = $avatar;
            if ($message != null)
                $data['content'] = $message;
            if ($embeds != null)
                $data["embeds"] = $embeds;
            $data_string = json_encode($data);
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string)
            ));
            $result = curl_exec($ch);
            $status = curl_getinfo($ch);
            if ($status['http_code'] == 204) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public function run(){
        $fetch = @file_get_contents("https://store.steampowered.com/appreviews/".$this->config->appid."?filter=recent&json=1&language=".(is_array($this->config->permitted_languages) && !empty($this->config->permitted_languages) && count($this->config->permitted_languages) == 1 ? $this->config->permitted_languages[0] : "all"));

        //TODO: filter of 'updated' could be used to retrieve updated reviews, most recently updated first
        //https://partner.steamgames.com/doc/store/getreviews
        if(!$fetch) die("Could not retrieve");
        $json = json_decode($fetch);
        if(empty($json)) die("Nothing to process");
        if($json->success != 1) die("Request failed");
        if($json->query_summary->num_reviews < 1) die("Nothing to process");

        $embeds = array();
        foreach($json->reviews as $review){
            if(!$this->config->debug){
                if($review->timestamp_created <= $this->lastran) break;
            }

            if(!empty($this->config->permitted_languages) && is_array($this->config->permitted_languages)){
                if(!in_array($review->language, $this->config->permitted_languages)){
                    continue;
                }
            }

            $fields = array(
                array(
                    "name" => "Outcome",
                    "value" => ($review->voted_up ? "Recommended :thumbsup:" : "Not Recommended :thumbsdown:"),
                    "inline" => true
                ),
                array(
                    "name" => "Played",
                    "value" => $this->hoursmins($review->author->playtime_forever).($review->author->playtime_last_two_weeks >0 && $review->author->playtime_last_two_weeks != $review->author->playtime_forever ? " (".$this->hoursmins($review->author->playtime_last_two_weeks)." in last 2 weeks)" : ""),
                    "inline" => true
                ),
                array(
                    "name" => "Description",
                    "value" => (strlen($review->review) > $this->config->review_char_limit ? substr($review->review, 0, $this->config->review_char_limit)."..." : $review->review),
                    "inline" => true
                )
            );
            $embeds[] = array(
                "title" => "Review #".$review->recommendationid,
                "url" => "https://steamcommunity.com/profiles/{$review->author->steamid}/recommended/".$this->config->appid."/",
                "color" => ($review->voted_up ? 45414 : 11610880),
                "timestamp" => date("Y-m-d\TH:i:s\Z", $review->timestamp_created),
                "fields" => $fields
            );
        }

        $all_embeds = array_chunk($embeds, 10);
        foreach($all_embeds as $embeds){
            $message = (count($embeds) == 1 ? "A new review came in:" : count($embeds)." new reviews came in:");
            $this->webhook_post($this->config->webhookURL, $this->config->avatar, $this->config->username, $embeds, $message);
            if($this->config->debug) print_r(json_encode(array('content' => $message, 'embeds' => $embeds)));
        }
    }

}