<?php
/**
 * StrevCord
 * @file run.php
 * @version 1.0
 * @author HilbertGilbertson
 * @url https://hilbertgilbertson.website
 */

require 'config.php';
require 'StrevCord.php';

$rc = new StrevCord($config);

date_default_timezone_set('UTC'); //should match the timezone of the remote server; leave as UTC unless you know better
$rc->run();

die("StrevCord Cron Executed");