# StrevCord v1.0

StrevCord is a PHP script for posting Steam review updates to a Discord webhook.

This is a basic implementation, crawling the Steam API every _x_ minutes, and posting new reviews into Discord.

StrevCord does not post to multiple channels, only looks at new reviews (old reviews that are updated are excluded), and will never post more than 20 recent reviews per call.

## Installation

### Upload files

Upload config.php, StrevCord.php and run.php to the same directory on your server. This could be a publicly accessible directory if you wish for the script to be able to be called externally; otherwise a directory above webroot is recommended.

### Configuration

Configure your settings in config.php, following the instructions within.

### Trial run

You can check if everything works by setting $config['debug'] in config.php to true and executing run.php. Warning: this may result in posting up to 20 of the most recent reviews! Presuming everything appears to work, don't forget to change $config['debug'] back to false.

### Cron

Configure a cron to execute run.php e.g. `*/10 * * * * /usr/bin/php /path/to/StrevCord/run.php >/dev/null 2>&1` or `*/5 * * * * wget -O - http://mysite.com/StrevCord/run.php >/dev/null 2>&1` if StrevCord is placed within webroot.

Note about access control: if you have uploaded the StrevCord directory within your webroot, it is recommended you use access control (such as with .htaccess) to limit access to specified IPs. You don't want strangers executing your cron.

## License

Released under MIT License

Copyright (c) 2018 HilbertGilbertson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
